package com.example.vladimir.androidacademymsk;

import dagger.Component;
import retrofit2.Retrofit;

@Component(modules = RetrofitModule.class)
public interface RestComponent {

    Retrofit getRetrofit();
}
