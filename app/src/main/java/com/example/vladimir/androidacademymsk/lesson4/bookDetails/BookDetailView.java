package com.example.vladimir.androidacademymsk.lesson4.bookDetails;

import com.arellomobile.mvp.MvpView;

public interface BookDetailView extends MvpView {

    void showData(String author, String title, String desc,String URL);
}
