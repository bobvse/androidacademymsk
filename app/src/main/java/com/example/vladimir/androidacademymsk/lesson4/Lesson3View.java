package com.example.vladimir.androidacademymsk.lesson4;

import com.arellomobile.mvp.MvpView;
import com.example.vladimir.androidacademymsk.lesson4.response.BookResponse;

import java.util.List;

public interface Lesson3View extends MvpView {
    void showProgress();

    void hideProgress();

    void showBooks(List<BookResponse> bookResponses);

    void showError(String error);

    void clearData();

    void loadPageBooks(List<BookResponse> bookResponses);
}

