package com.example.vladimir.androidacademymsk.lesson2;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.example.vladimir.androidacademymsk.R;

import java.util.List;


public class Lesson2Activity extends AppCompatActivity implements View.OnClickListener {
    private static final String VK_APP_PACKAGE_ID = "com.vkontakte.android";
    private static final String FACEBOOK_APP_PACKAGE_ID = "com.facebook.katana";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lesson2_activity);
        initToolbar();
        initViews();
        //test

    }

    private void initToolbar() {
            Toolbar toolbar = findViewById(R.id.toolbar);
            toolbar.setTitle(getString(R.string.profile));
            setSupportActionBar(toolbar);

//Для себя, что бы это работало необходимо в манифесте указать родителя
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //Альтернативный способ
//        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });
    }

    private void initViews() {
        findViewById(R.id.lesson2_activity_makeCall_btn).setOnClickListener(this);
        findViewById(R.id.lesson2_activity_sendEmail_btn).setOnClickListener(this);
        findViewById(R.id.lesson2_activity_vk_btn).setOnClickListener(this);
        findViewById(R.id.lesson2_activity_telegram_btn).setOnClickListener(this);
        findViewById(R.id.lesson2_activity_whatsapp_btn).setOnClickListener(this);
    }

    private void makeCall() {
        String phone = getText(R.string.phone).toString();
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Toast.makeText(this, "No Phone app found", Toast.LENGTH_LONG).show();
        }
    }

    private void sendEmail() {
        Intent intent = new Intent(Intent.ACTION_SENDTO);

        intent.setData(Uri.parse(String.format("mailto:%s", getString(R.string.email_address))));
        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.email_subject));

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(Intent.createChooser(intent, "Send Email"));
        } else {
            Toast.makeText(this, "No Email app found", Toast.LENGTH_LONG).show();
        }
    }

    public static void openLink(AppCompatActivity activity, String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        List<ResolveInfo> resInfo = activity.getPackageManager().queryIntentActivities(intent, 0);

        if (resInfo.isEmpty()) {
            activity.startActivity(intent);
        }

        for (ResolveInfo info : resInfo) {
            if (info.activityInfo == null) {
                continue;
            }
            if (VK_APP_PACKAGE_ID.equals(info.activityInfo.packageName)
                    || FACEBOOK_APP_PACKAGE_ID.equals(info.activityInfo.packageName)
                    ) {
                intent.setPackage(info.activityInfo.packageName);
                break;
            }
        }
        activity.startActivity(intent);
    }

    void intentMessageTelegram() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("http://telegram.me/@bobvse"));
        final String appName = "org.telegram.messenger";
        if (isAppAvailable(this.getApplicationContext(), appName)) {
            intent.setPackage(appName);
        }


        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Toast.makeText(this, "No Available app found", Toast.LENGTH_LONG).show();
        }
    }

    public static boolean isAppAvailable(Context context, String appName) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(appName, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private void sendWhatsApp() {
        String url = "https://api.whatsapp.com/send?phone=" + getString(R.string.phone);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Toast.makeText(this, "No WhatsApp app found", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.lesson2_activity_makeCall_btn:
                makeCall();
                break;
            case R.id.lesson2_activity_sendEmail_btn:
                sendEmail();
                break;
            case R.id.lesson2_activity_vk_btn:
                openLink(this, "https://m.vk.com/id7736364");
                break;
            case R.id.lesson2_activity_telegram_btn:
                intentMessageTelegram();
                break;
            case R.id.lesson2_activity_whatsapp_btn:
                sendWhatsApp();
                break;
        }
    }
}
