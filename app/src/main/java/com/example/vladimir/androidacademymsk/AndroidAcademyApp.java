package com.example.vladimir.androidacademymsk;

import android.app.Application;

public class AndroidAcademyApp extends Application {

   private static RestComponent component;

    public static RestComponent getComponent(){
        return component;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        component = buildComponent();
    }

    protected RestComponent buildComponent(){
        RestComponent component = DaggerRestComponent.builder()
                .retrofitModule(new RetrofitModule())
                .build();
        return component;
    }

}
