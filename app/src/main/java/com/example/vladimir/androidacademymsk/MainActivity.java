package com.example.vladimir.androidacademymsk;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.vladimir.androidacademymsk.lesson1.Lesson1ActivityFirst;
import com.example.vladimir.androidacademymsk.lesson2.Lesson2Activity;
import com.example.vladimir.androidacademymsk.lesson4.Lesson3Activity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        initViews();
        initToolbar();
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void initViews(){
        findViewById(R.id.main_activity_btn_lesson1).setOnClickListener(this);
        findViewById(R.id.main_activity_btn_lesson2).setOnClickListener(this);
        findViewById(R.id.main_activity_btn_lesson3).setOnClickListener(this);
        findViewById(R.id.main_activity_btn_lesson4).setOnClickListener(this);
        findViewById(R.id.main_activity_btn_lesson5).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.main_activity_btn_lesson1:
                startActivity(new Intent(this, Lesson1ActivityFirst.class));
                break;
            case R.id.main_activity_btn_lesson2:
                startActivity(new Intent(this, Lesson2Activity.class));
                break;
            case R.id.main_activity_btn_lesson3:
                startActivity(new Intent(this,Lesson3Activity.class));
                break;
            case R.id.main_activity_btn_lesson4:
                //TODO доделать
                break;
            case R.id.main_activity_btn_lesson5:
                //TODO доделать
                break;
        }
    }
}
