package com.example.vladimir.androidacademymsk.lesson1;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vladimir.androidacademymsk.R;


public class Lesson1SecondActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lesson1_second_activity);

        initViews();
        getData();
    }

    private void initViews() {
        message = findViewById(R.id.lesson1_text_view_second_activity);
        findViewById(R.id.lesson1_send_message_button).setOnClickListener(this);
    }

    private void getData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey(Lesson1ActivityFirst.LESSON1_ACTIVITY_EXTRA_KEY)) {
                message.setText(extras.getString(Lesson1ActivityFirst.LESSON1_ACTIVITY_EXTRA_KEY));
            }
        }
    }

    private void sendEmail(String message) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);

        intent.setData(Uri.parse(String.format("mailto:%s", getString(R.string.email_address))));
        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.email_subject));
        intent.putExtra(Intent.EXTRA_TEXT, message);

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(Intent.createChooser(intent, "Send Email"));
        } else {
            Toast.makeText(this, "No Email app found", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.lesson1_send_message_button:
                sendEmail(message.getText().toString());
                break;
        }
    }
}
