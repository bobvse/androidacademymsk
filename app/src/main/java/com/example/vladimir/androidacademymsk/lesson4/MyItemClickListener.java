package com.example.vladimir.androidacademymsk.lesson4;

import com.example.vladimir.androidacademymsk.lesson4.response.BookResponse;

public interface MyItemClickListener {

    void onItemClick(BookResponse bookResponse);
}
