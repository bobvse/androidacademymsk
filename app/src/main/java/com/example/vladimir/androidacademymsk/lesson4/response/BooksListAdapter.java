package com.example.vladimir.androidacademymsk.lesson4.response;

import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vladimir.androidacademymsk.lesson4.MyItemClickListener;
import com.example.vladimir.androidacademymsk.R;
import com.squareup.picasso.Picasso;

import java.util.Arrays;
import java.util.List;

public class BooksListAdapter extends RecyclerView.Adapter<BooksListAdapter.ViewHolder> {
    private List<BookResponse> booksList;
    private Context mContext;
    private MyItemClickListener listener;


    public BooksListAdapter(List<BookResponse> booksList, MyItemClickListener listener, Context context) {
        this.booksList = booksList;
        this.listener = listener;
        this.mContext = context;
    }

    public BooksListAdapter(Context context,List<BookResponse> booksList) {
        this.booksList = booksList;
        this.mContext = context;
    }

    public void setData(List<BookResponse> booksList) {
        this.booksList = booksList;
        notifyDataSetChanged();
    }

    public void addData(List<BookResponse> booksList) {
        this.booksList.addAll(booksList);
        notifyDataSetChanged();
    }

    public void clearData() {
        booksList.clear();
        notifyDataSetChanged();
    }

    @Override
    public BooksListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.book_card, parent, false);
        final ViewHolder mViewHolder = new ViewHolder(mView);

        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.title.setText(booksList.get(position).getVolumeInfo().getTitle());
        if(booksList.get(position).getVolumeInfo().getAuthors()==null){
            holder.author.setText("Автор неизвестен");
        }else{
            holder.author.setText(Arrays.toString(booksList.get(position).getVolumeInfo().getAuthors()));
        }

        holder.picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(booksList.get(position));
            }
        });

        holder.oferflow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopupMenu(holder.oferflow);
            }
        });

        if (booksList.get(position).getVolumeInfo().getImageLinks() != null) {
            Picasso.get()
                    .load(booksList.get(position).getVolumeInfo().getImageLinks().getThumbnail())
                    //.resize(80, 80)
                    .centerCrop()
                    .fit()
                    .into(holder.picture);

        } else {
           // holder.picture.setImageResource(R.drawable.common_google_signin_btn_text_dark);
        }
    }

    @Override
    public int getItemCount() {
        if (booksList == null) {
            return 0;
        } else {
            return booksList.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView picture;
        public TextView title;
        public TextView author;
        public ImageView oferflow;


        ViewHolder(View v) {
            super(v);
            title = v.findViewById(R.id.list_title);
            picture = v.findViewById(R.id.list_avatar);
            author = v.findViewById(R.id.list_desc);
            oferflow = v.findViewById(R.id.overflow);
        }
    }

    /**
     * Showing popup menu when tapping on 3 dots
     */
    private void showPopupMenu(View view) {
        // inflate menu
        PopupMenu popup = new PopupMenu(mContext, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_book, popup.getMenu());
        popup.setOnMenuItemClickListener(new MyMenuItemClickListener());
        popup.show();
    }

    /**
     * Click listener for popup menu items
     */
    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        public MyMenuItemClickListener() {
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.action_add_favourite:
                    Toast.makeText(mContext, "Add to favourite", Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.action_play_next:
                    Toast.makeText(mContext, "Buy", Toast.LENGTH_SHORT).show();
                    return true;
                default:
            }
            return false;
        }
    }
}

