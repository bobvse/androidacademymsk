package com.example.vladimir.androidacademymsk.lesson1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.example.vladimir.androidacademymsk.R;

public class Lesson1ActivityFirst extends AppCompatActivity implements View.OnClickListener{
    public final static String LESSON1_ACTIVITY_EXTRA_KEY = "key_lesson1";
private EditText message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lesson1_first_activity);

        initViews();
    }

    private void initViews(){
        message =  findViewById(R.id.lesson1_activity_et);
        findViewById(R.id.lesson1_activity_btn_send).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.lesson1_activity_btn_send:
                Intent intent = new Intent(this,Lesson1SecondActivity.class);
                intent.putExtra(LESSON1_ACTIVITY_EXTRA_KEY,message.getText().toString());
                startActivity(intent);
                break;
        }
    }
}
