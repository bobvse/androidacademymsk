package com.example.vladimir.androidacademymsk.lesson4;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.vladimir.androidacademymsk.AndroidAcademyApp;
import com.example.vladimir.androidacademymsk.lesson4.response.BooksResponse;
import com.example.vladimir.androidacademymsk.GoogleBooksApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@InjectViewState
public class Lesson3Presenter extends MvpPresenter<Lesson3View> {

    private final static String API_KEY = "AIzaSyDYgqEsjNkKtFJtixnuJ7t_aWXEVb83VmI";
    private String query;


    public Lesson3Presenter() {

    }


    public void loadBooks(String searchBook) {
        query = searchBook;

        GoogleBooksApi api =  AndroidAcademyApp.getComponent().getRetrofit().create(GoogleBooksApi.class);
        getViewState().showProgress();
        api.getBooksList(searchBook, API_KEY, 40).enqueue(new Callback<BooksResponse>() {
            @Override
            public void onResponse(Call<BooksResponse> call, Response<BooksResponse> response) {
                BooksResponse booksResponse = response.body();
                if (booksResponse.getBooks() == null) {
                    getViewState().hideProgress();
                    getViewState().clearData();
                    getViewState().showError("Ничего не найдено");
                } else {
                    getViewState().showBooks(booksResponse.getBooks());
                    getViewState().hideProgress();
                }
            }

            @Override
            public void onFailure(Call<BooksResponse> call, Throwable t) {
                String error = t.getMessage();
                getViewState().clearData();
                getViewState().showError(error);
            }
        });
    }

    public void loadBooksPagination(int index) {

        GoogleBooksApi api =  AndroidAcademyApp.getComponent().getRetrofit().create(GoogleBooksApi.class);
        api.getBooksListPagination(query, API_KEY, 40, index).enqueue(new Callback<BooksResponse>() {
            @Override
            public void onResponse(Call<BooksResponse> call, Response<BooksResponse> response) {
                BooksResponse booksResponse = response.body();
                if (booksResponse.getBooks() == null) {
                    //ничего не делать
                } else {
                    getViewState().loadPageBooks(booksResponse.getBooks());
                }
            }

            @Override
            public void onFailure(Call<BooksResponse> call, Throwable t) {
                String error = t.getMessage();
                getViewState().clearData();
                getViewState().showError(error);
            }
        });
    }
}
